export PACKAGE := profphd
export VERSION := 1.0.42
export PACKAGE_VERSION := $(VERSION)
export PACKAGE_STRING := $(PACKAGE) $(PACKAGE_VERSION)
export DISTDIR := $(PACKAGE)-$(VERSION)

SRCDIR := src
#SRCPROF=$(SRCDIR)/src-prof
#SRCPHD=$(SRCDIR)/src-phd
#MAX_LIB := lib-maxhom.f
# lkajan: ARCH seems to be used when OS is meant: LINUX, MAC, etc. However it may indeed try to capture the architecture /as well/. Guy recommends to set it to LINUX by default.
# Binaries will not have the .$(ARCH) extension any more.
#ARCH := LINUX
#SYS_LIB := lib-sys-$(ARCH).f

#=====================================================================
#F77			=	f77
#=====================================================================
#FFLAGS			=	"-O3 -Wcharacter-truncation -Wsurprising -Waliasing -finit-local-zero -fbounds-check"
#FFLAGS          	= 	-O3 -C -finit-local-zero
#FFLAGS          	=  	-O3 -ftz -fno-alias -ip -finit-local-zero
#=====================================================================
#SUBMAKEARGS=ARCH=$(ARCH) FFLAGS=$(FFLAGS)
#=====================================================================
all:	prof

prof:
	$(MAKE) -C src/prof

#=====================================================================
# Make prof
#=====================================================================
#prof:
#	$(MAKE) -C $(SRCPROF) $(SUBMAKEARGS)
#
#PROF_OBJS=$(SRCPROF)/prof.f $(SRCPROF)/lib-prof.f $(SRCPROF)/$(SYS_LIB)
#	$(F77) -o  $@.$(ARCH) $(FFLAGS) $(PROF_OBJS)
#prof.o : =$(SRCPROF)/profPar.f

#=====================================================================
# Make phd
#=====================================================================
#phd:
#	$(MAKE) -C $(SRCPHD) $(SUBMAKEARGS)
#
#PHD_OBJS= $(SRCPHD)/phd.f $(SRCPHD)/lib-phd.f
#phd: $(OBJS);
#	$(F77) $(FFLAGS) -o $@.$(ARCH) $(PHD_OBJS)
#phd.o: phdParameter.f

distclean: clean
	rm -rf\
		$(DISTDIR) \
		$(DISTDIR).tar.gz \
		profphd-utils-*.tar.gz

clean:
	rm -f prof.tar *.[1-9] *.[1-9].gz
	$(MAKE) -C src/prof clean

INSTALL = ./packaging_scripts/instcopy -c
INSTALL_PROGRAM = $(INSTALL) -m 755

dist: dist-profphd

dist-profphd:	$(DISTDIR)
	tar -c -f - "$(DISTDIR)" | xz -c >$(DISTDIR).tar.xz
	rm -rf $(DISTDIR)

$(DISTDIR): distclean
	rm -rf $(DISTDIR) && mkdir -p $(DISTDIR) && \
	rsync -avC \
		--exclude /*-stamp \
		--exclude .*.swp \
		--exclude *.1 \
		--exclude Session.vim \
		AUTHORS \
		COPYING \
		ChangeLog \
		INSTALL \
		Makefile \
		$(PACKAGE).spec \
		README \
		src \
		$(DISTDIR)/;

install-man:
	$(MAKE) -C src/prof install-man

install: install-man install-perl install-neuralnet

install-perl:
	$(MAKE) -C src/prof install-perl

install-neuralnet:
	$(MAKE) -C src/prof install-neuralnet

help:
	@echo "Targets:"
	@echo "all* - build prof package (no profphd-utils)"
	@echo "bin - make profphd-utils package (working copy make only)"
	@echo "clean"
	@echo "dist - dist-profphd"
	@echo "dist-profphd - prepare distributable tar.gz of src/prof"
	@echo "  WC make only"
	@echo "distclean"
	@echo "prof"
	@echo "install - install-man install-perl install-neuralnet"
	@echo "install-bin - install profphd-utils package (working copy make only)"
	@echo "install-man"
	@echo "install-perl"
	@echo "install-neuralnet"
	@echo
	@echo "Variables:"
	@echo "DESTDIR - install to DESTDIR"
	@echo "prefix - common installation prefix for all files"
	@echo "  use prefix=$$HOME to build for personal use"

.PHONY: all bin dist convert_seq filter_hssp prof install install-man install-perl install-neuralnet help 

# vim:ai:
