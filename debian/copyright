Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: profphd
Upstream-Contact: Laszlo Kajan <lkajan@rostlab.org>
Source: ftp://rostlab.org/profphd/

Files: *
Copyright:
    1998 B. Rost <rost@rostlab.org> EMBL, CUBIC (Columbia University, NY, USA) and LION Biosciences (Heidelberg, DE)
    C. Sander
    P. Fariselli
    R. Casadio
    J. Liu
    2009-2013 G. Yachdav <gyachdav@rostlab.org> CUBIC (Columbia University, NY, USA) and Technical University Munich (Munich, DE)
    2009-2013 L. Kajan <lkajan@rostlab.org> Laszlo Kajan <lkajan@rostlab.org> Technical University Munich (Munich, DE)
License: GPL-2+

Files: src/prof/embl/scr/ctime.pl src/prof/embl/scr/lib/ctime.pl
Copyright: 1988 Waldemar Kebsch <kebsch.pad@nixpbe.UUCP>
           March 1990, Feb 1991 Marion Hakanson <hakanson@cse.ogi.edu>, Oregon Graduate Institute of Science and Technology
License: GPL-1+ or Artistic

Files: debian/*
Copyright: 2009 Laszlo Kajan <lkajan@rostlab.org>
License: GPL-2+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in `/usr/share/common-licenses/GPL-2'.
