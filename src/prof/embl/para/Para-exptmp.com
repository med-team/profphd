*************************************************************************************
*****                                                                           *****
***** Secondary Structure Prediction by a Layered Network                       *****
***** ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~                       *****
*****                                                                           *****
***** Burkhard Rost, EMBL, Heidelberg, FRG                                      *****
***** (Internet: rost@EMBL-Heidelberg.DE)                                       *****
*****                                                                           *****
***** Wed Oct 14 13:57:04 1992                                                  *****
*****                                                                           *****
***** This file contains the required parameters.                               *****
*****                                                                           *****
*************************************************************************************
*****                                                                           *****
*****
 PATH_ARCH          
*****
 MAXVAR=               150
 MAXACC=               150
 PROFACC=                4
 CASCACC=                8
 NBIOLOBJ               21
*****
 NUMNETFST=              1
 NUMNETSND=              0
 NUMNETJURY=             1
*****
 MODEASSCAS(1:NUMNETFST) (row: no. A25)
         1          P-REAL-CICLD
*****
 FILEARCHFST(1:NUMNETFST) (row: no. A50)
         1          Exp152a-w9-15rcicld.DAT
END
