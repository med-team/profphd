*************************************************************************************
*****                                                                           *****
***** Secondary Structure Prediction by a Layered Network                       *****
***** ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~                       *****
*****                                                                           *****
***** Burkhard Rost, EMBL, Heidelberg, FRG                                      *****
***** (Internet: rost@EMBL-Heidelberg.DE)                                       *****
*****                                                                           *****
***** Tue Oct 25 23:57:04 1994                                                  *****
*****                                                                           *****
***** This file contains the required parameters.                               *****
*****                                                                           *****
*************************************************************************************
*****                                                                           *****
*****
 PATH_ARCH          __PREFIX__/share/profphd/prof/embl/net/
*****
 MAXVAR=               150
 MAXACC=               150
 PROFACC=                4
 CASCACC=                8
 NBIOLOBJ               21
*****
 NUMNETFST=              5
 NUMNETSND=              0
 NUMNETJURY=             5
*****
 MODEASSCAS(1:NUMNETFST) (row: no. A25)
         1          P-REAL-CCLD
         2          P-REAL-CIC
         3          P-REAL-CICLD
         4          P-REAL-CCLD
         5          P-REAL-CICLD
*****
 FILEARCHFST(1:NUMNETFST) (row: no. A50)
         1          Exp450x-rccld.DAT
         2          Exp450x-rcic.DAT
         3          Exp450x-rcicld.DAT
         4          Exp450x-w9-rccld.DAT
         5          Exp450x-w9-rcicld.DAT
END

