*************************************************************************************
*****                                                                           *****
***** Secondary Structure Prediction by a Layered Network                       *****
***** ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~                       *****
*****                                                                           *****
***** Burkhard Rost, EMBL, Heidelberg, FRG                                      *****
***** (Internet: rost@EMBL-Heidelberg.DE)                                       *****
*****                                                                           *****
***** Tue Oct 25 23:57:04 1994                                                  *****
*****                                                                           *****
***** This file contains the required parameters.                               *****
*****                                                                           *****
*************************************************************************************
*****                                                                           *****
*****
 PATH_ARCH          __PREFIX__/share/profphd/prof/embl/net/
*****
 MAXVAR=               150
 MAXACC=               150
 PROFACC=                4
 CASCACC=                8
 NBIOLOBJ               21
*****
 NUMNETFST=             15
 NUMNETSND=              0
 NUMNETJURY=            15
*****
 MODEASSCAS(1:NUMNETFST) (row: no. A25)
         1          P-REAL-CCLD
         2          P-REAL-CIC
         3          P-REAL-CICLD
         4          P-REAL-CCLD
         5          P-REAL-CICLD
         6          P-REAL-CCLD
         7          P-REAL-CIC
         8          P-REAL-CICLD
         9          P-REAL-CCLD
        10          P-REAL-CICLD
        11          P-REAL-CCLD
        12          P-REAL-CIC
        13          P-REAL-CICLD
        14          P-REAL-CCLD
        15          P-REAL-CICLD
*****
 FILEARCHFST(1:NUMNETFST) (row: no. A50)
         1          Exp152x-15rccld.DAT
         2          Exp152x-15rcic.DAT
         3          Exp152x-15rcicld.DAT
         4          Exp152x-w9-15rccld.DAT
         5          Exp152x-w9-15rcicld.DAT
         6          Exp317x-15rccld.DAT
         7          Exp317x-15rcic.DAT
         8          Exp317x-15rcicld.DAT
         9          Exp317x-w9-15rccld.DAT
        10          Exp317x-w9-15rcicld.DAT
        11          Exp450x-rccld.DAT
        12          Exp450x-rcic.DAT
        13          Exp450x-rcicld.DAT
        14          Exp450x-w9-rccld.DAT
        15          Exp450x-w9-rcicld.DAT
END

