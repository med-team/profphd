*************************************************************************************
*****                                                                           *****
***** Secondary Structure Prediction by a Layered Network                       *****
***** ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~                       *****
*****                                                                           *****
***** Burkhard Rost, EMBL, Heidelberg, FRG                                      *****
***** (Internet: rost@EMBL-Heidelberg.DE)                                       *****
*****                                                                           *****
***** Wed Oct 14 13:57:04 1992                                                  *****
*****                                                                           *****
***** This file contains the required parameters.                               *****
*****                                                                           *****
*************************************************************************************
*****                                                                           *****
*****
 PATH_ARCH          __PREFIX__/share/profphd/prof/embl/net/
*****
 MAXVAR=               150
 MAXACC=               150
 PROFACC=                4
 CASCACC=                8
 NBIOLOBJ               21
*****
 NUMNETFST=              2
 NUMNETSND=             10
 NUMNETJURY=            10
*****
 TRANS2FROM1(1:NUMNETSND) (20I4)
   1   1   1   1   1   1   2   2   2   2
*****
 MODEASSCAS(1:NUMNETFST) (row: no. A25)
         1          P-REAL-CICLD
         2          P-REAL-CONS-INDEL
*****
 MODEASSSTR(1:NUMNETSND) (row: no. A25)
         1          REAL-CCLD
         2          REAL-CCLD
         3          REAL-CC
         4          REAL-CC
         5          REAL-CONS93
         6          REAL-CONS93
         7          REAL-CCLD
         8          REAL-CCLD
         9          REAL-CONS93
        10          REAL-CONS93
*****
 FILEARCHFST(1:NUMNETFST) (row: no. A50)
         1          F317x-pcicld.DAT
         2          F317x-pci.DAT
 FILEASND(1:NUMNETSND) (row: no. A50)
         1          S317x-pccld-pcicld.DAT
         2          S317x-rccld-pcicld.DAT
         3          S317x-pcc-pcicld.DAT
         4          S317x-rcc-pcicld.DAT
         5          S317x-pc-pcicld.DAT
         6          S317x-rc-pcicld.DAT
         7          S317x-pccld-pci.DAT
         8          S317x-rccld-pci.DAT
         9          S317x-pc-pci.DAT
        10          S317x-rc-pci.DAT
END
