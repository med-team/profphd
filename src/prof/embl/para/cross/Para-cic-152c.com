*************************************************************************************
*****                                                                           *****
***** Secondary Structure Prediction by a Layered Network                       *****
***** ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~                       *****
*****                                                                           *****
***** Burkhard Rost, EMBL, Heidelberg, FRG                                      *****
***** (Internet: rost@EMBL-Heidelberg.DE)                                       *****
*****                                                                           *****
***** Wed Oct 14 13:57:04 1992                                                  *****
*****                                                                           *****
***** This file contains the required parameters.                               *****
*****                                                                           *****
*************************************************************************************
*****                                                                           *****
*****
 PATH_ARCH          
*****
 MAXVAR=               150
 MAXACC=               150
 PROFACC=                4
 CASCACC=                8
 NBIOLOBJ               21
*****
 NUMNETFST=              3
 NUMNETSND=             10
 NUMNETJURY=            10
*****
 TRANS2FROM1(1:NUMNETSND) (20I4)
   1   1   2   2   2   2   3   3   3   3
*****
 MODEASSCAS(1:NUMNETFST) (row: no. A25)
         1          PROFILE-REAL-CONS
         2          P-REAL-CONS-INDEL
         3          P-REAL-CONS-INCOM
*****
 MODEASSSTR(1:NUMNETSND) (row: no. A25)
         1          REAL-CONS
         2          REAL-CONS
         3          REAL-CONS93
         4          REAL-CONS93
         5          REAL-OCT
         6          REAL-OCT
         7          REAL-CONS93
         8          REAL-CONS93
         9          REAL-CONS-COMPO
        10          REAL-CONS-COMPO
*****
 FILEARCHFST(1:NUMNETFST) (row: no. A50)
         1          F152c-15pc.DAT
         2          F152c-15pci.DAT
         3          F152c-15pcic.DAT
 FILEASND(1:NUMNETSND) (row: no. A50)
         1          S152c-15rc-15pc.DAT
         2          S152c-15pc-15pc.DAT
         3          S152c-15r-15pci.DAT
         4          S152c-15p-15pci.DAT
         5          S152c-15rc-15pci.DAT
         6          S152c-15pc-15pci.DAT
         7          S152c-15rc-15pcic.DAT
         8          S152c-15pc-15pcic.DAT
         9          S152c-15rcc-15pcic.DAT
        10          S152c-15pcc-15pcic.DAT
END
