*************************************************************************************
*****                                                                           *****
***** Helical Transmembrane Segment Prediction by a Layered Network             *****
***** ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~             *****
*****                                                                           *****
***** Burkhard Rost, EMBL, Heidelberg, FRG                                      *****
***** (Internet: rost@EMBL-Heidelberg.DE)                                       *****
*****                                                                           *****
***** Wed Aug 14 13:57:04 1994                                                  *****
*****                                                                           *****
***** This file contains the required parameters.                               *****
*****                                                                           *****
*************************************************************************************
*****                                                                           *****
*****
 PATH_ARCH          
*****
 MAXVAR=               150
 MAXACC=               150
 PROFACC=                4
 CASCACC=                8
 NBIOLOBJ               21
*****
 NUMNETFST=              2
 NUMNETSND=              4
 NUMNETJURY=             4
*****
 TRANS2FROM1(1:NUMNETSND) (20I4)
   1   1   2   2
*****
 MODEASSCAS(1:NUMNETFST) (row: no. A25)
         1          P-REAL-CICLD
         2          P-REAL-CICLD
*****
 MODEASSSTR(1:NUMNETSND) (row: no. A25)
         1          RR-CCLD
         2          RR-CCLD
         3          RR-CCLD
         4          RR-CCLD
*****
 FILEARCHFST(1:NUMNETFST) (row: no. A50)
         1          Tm69c-rcicld.DAT
         1          Tm69c-pcicld.DAT
 FILEASND(1:NUMNETSND) (row: no. A50)
         1          Stm69c-rccld-rcicld.DAT
         2          Stm69c-pccld-rcicld.DAT
         3          Stm69c-rccld-pcicld.DAT
         4          Stm69c-pccld-pcicld.DAT
END
