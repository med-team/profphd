*************************************************************************************
*****                                                                           *****
***** Secondary Structure Prediction by a Layered Network                       *****
***** ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~                       *****
*****                                                                           *****
***** Burkhard Rost, EMBL, Heidelberg, FRG                                      *****
***** (Internet: rost@EMBL-Heidelberg.DE)                                       *****
*****                                                                           *****
***** Wed Oct 14 13:57:04 1992                                                  *****
*****                                                                           *****
***** This file contains the required parameters.                               *****
*****                                                                           *****
*************************************************************************************
*****                                                                           *****
*****
 PATH_ARCH          
*****
 MAXVAR=               150
 MAXACC=               150
 PROFACC=                4
 CASCACC=                8
 NBIOLOBJ               21
*****
 NUMNETFST=              8
 NUMNETSND=              0
 NUMNETJURY=             8
*****
 MODEASSCAS(1:NUMNETFST) (row: no. A25)
         1          P-REAL-CONS
         2          P-REAL-CIC
         3          P-REAL-CICLD
         4          P-REAL-CONS
         5          P-REAL-CC
         6          P-REAL-CCLD
         7          P-REAL-CIC
         8          P-REAL-CICLD
*****
 FILEARCHFST(1:NUMNETFST) (row: no. A50)
         1          Exp152c-15rc.DAT
         2          Exp152c-15rcic.DAT
         3          Exp152c-15rcicld.DAT
         4          Exp152c-w9-15rc.DAT
         5          Exp152c-w9-15rcc.DAT
         6          Exp152c-w9-15rccld.DAT
         7          Exp152c-w9-15rcic.DAT
         8          Exp152c-w9-15rcicld.DAT
END
         1          Exp152c-15rc.DAT
         2          Exp152c-15rcic.DAT
         3          Exp152c-15rcicld.DAT
         4          Exp152c-w9-15rc.DAT
         5          Exp152c-w9-15rcc.DAT
         6          Exp152c-w9-15rccld.DAT
         7          Exp152c-w9-15rcic.DAT
         8          Exp152c-w9-15rcicld.DAT

