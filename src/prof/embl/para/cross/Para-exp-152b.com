*************************************************************************************
*****                                                                           *****
***** Secondary Structure Prediction by a Layered Network                       *****
***** ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~                       *****
*****                                                                           *****
***** Burkhard Rost, EMBL, Heidelberg, FRG                                      *****
***** (Internet: rost@EMBL-Heidelberg.DE)                                       *****
*****                                                                           *****
***** Wed Oct 14 13:57:04 1992                                                  *****
*****                                                                           *****
***** This file contains the required parameters.                               *****
*****                                                                           *****
*************************************************************************************
*****                                                                           *****
*****
 PATH_ARCH          
*****
 MAXVAR=               150
 MAXACC=               150
 PROFACC=                4
 CASCACC=                8
 NBIOLOBJ               21
*****
 NUMNETFST=              7
 NUMNETSND=              0
 NUMNETJURY=             7
*****
 MODEASSCAS(1:NUMNETFST) (row: no. A25)
         1          P-REAL-CONS
         2          P-REAL-CIC
         3          P-REAL-CICLD
         4          P-REAL-CONS
         5          P-REAL-CCLD
         6          P-REAL-CIC
         7          P-REAL-CICLD
*****
 FILEARCHFST(1:NUMNETFST) (row: no. A50)
         1          Exp152b-15rc.DAT
         2          Exp152b-15rcic.DAT
         3          Exp152b-15rcicld.DAT
         4          Exp152b-w9-15rc.DAT
         5          Exp152b-w9-15rccld.DAT
         6          Exp152b-w9-15rcic.DAT
         7          Exp152b-w9-15rcicld.DAT
END
         1          Exp152b-15rc.DAT
         2          Exp152b-15rcic.DAT
         3          Exp152b-15rcicld.DAT
         4          Exp152b-w9-15rc.DAT
         5          Exp152b-w9-15rcc.DAT
         6          Exp152b-w9-15rccld.DAT
         7          Exp152b-w9-15rcic.DAT
         8          Exp152b-w9-15rcicld.DAT

