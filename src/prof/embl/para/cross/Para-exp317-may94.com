*************************************************************************************
*****                                                                           *****
***** Secondary Structure Prediction by a Layered Network                       *****
***** ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~                       *****
*****                                                                           *****
***** Burkhard Rost, EMBL, Heidelberg, FRG                                      *****
***** (Internet: rost@EMBL-Heidelberg.DE)                                       *****
*****                                                                           *****
***** Wed Oct 14 13:57:04 1992                                                  *****
*****                                                                           *****
***** This file contains the required parameters.                               *****
*****                                                                           *****
*************************************************************************************
*****                                                                           *****
*****
 PATH_ARCH          
*****
 MAXVAR=               150
 MAXACC=               150
 PROFACC=                4
 CASCACC=                8
 NBIOLOBJ               21
*****
 NUMNETFST=              5
 NUMNETSND=              0
 NUMNETJURY=             5
*****
 MODEASSCAS(1:NUMNETFST) (row: no. A25)
         1          P-REAL-CCLD
         2          P-REAL-CIC
         3          P-REAL-CICLD
         4          P-REAL-CCLD
         5          P-REAL-CICLD
*****
 FILEARCHFST(1:NUMNETFST) (row: no. A50)
         1          Exp317x-15rccld.DAT
         2          Exp317x-15rcic.DAT
         3          Exp317x-15rcicld.DAT
         4          Exp317x-w9-15rccld.DAT
         5          Exp317x-w9-15rcicld.DAT
END

