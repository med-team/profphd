#                                                                      #
# ==================================================================== #
# PROF predictions for 1ppt                                            #
# ==================================================================== #
#                                                                      #
# -------------------------------------------------------------------- #
# SYNOPSIS of prediction                                               #
# -------------------------------------------------------------------- #
#                                                                      #
# PROFsec summary                                                      #
#                                                                      #
# overall your protein can be classified as:                           #
#                                                                      #
# >>>   all-alpha  <<<                                                 #
#                                                                      #
# given the following classes:                                         #
# 'all-alpha':   %H > 45% AND %E <  5%                                 #
# 'all-beta':    %H <  5% AND %E > 45%                                 #
# 'alpha-beta':  %H > 30% AND %E > 20%                                 #
# 'mixed':       all others                                            #
#                                                                      #
# Predicted secondary structure composition:                           #
# +-----------------+-------+-------+-------+                          #
# | sec str type    |     H |     E |     L |                          #
# | % in protein    |  52.8 |   0.0 |  47.2 |                          #
# +-----------------+-------+-------+-------+                          #
#                                                                      #
# -------------------------------------------------------------------- #
# HEADER information                                                   #
# -------------------------------------------------------------------- #
#                                                                      #
# ...................                                                  #
# About your protein:                                                  #
# ...................                                                  #
#                                                                      #
# prot_id        : 1ppt                                                #
# prot_name      :                                                     #
# prot_nres      : 36                                                  #
# prot_nali      : 31                                                  #
# prot_nchn      : 1                                                   #
# prot_nfar      : 26                                                  #
#                                                                      #
# .........................                                            #
# About the alignment used:                                            #
# .........................                                            #
#                                                                      #
# ali_orig       : 2ppt.fastamul                                       #
# ali_used       : PROF1471452ppt.hssp                                 #
# ali_para       : 0                                                   #
#                                                                      #
# .....................................                                #
# Residue composition for your protein:                                #
# .....................................                                #
#                                                                      #
# +-----------------+-------+-------+-------+-------+-------+          #
# | amino acid type |     A |     C |     D |     E |     F |          #
# | % in protein    |   2.8 |   0.0 |  11.1 |   2.8 |   2.8 |          #
# +-----------------+-------+-------+-------+-------+-------+          #
# | amino acid type |     G |     H |     I |     K |     L |          #
# | % in protein    |   5.6 |   2.8 |   2.8 |   0.0 |   8.3 |          #
# +-----------------+-------+-------+-------+-------+-------+          #
# | amino acid type |     M |     N |     P |     Q |     R |          #
# | % in protein    |   0.0 |   5.6 |  11.1 |   8.3 |   8.3 |          #
# +-----------------+-------+-------+-------+-------+-------+          #
# | amino acid type |     S |     T |     V |     W |     Y |          #
# | % in protein    |   2.8 |   5.6 |   8.3 |   0.0 |  11.1 |          #
# +-----------------+-------+-------+-------+-------+-------+          #
#                                                                      #
# .............................                                        #
# About  the PROF methods used:                                        #
# .............................                                        #
#                                                                      #
# prof_fpar      : sec=/nfs/home1/yachdav/work/SNAP/prof/net/PROFsec_best.par        #
# prof_nnet      : sec=2                                               #
# prof_mode      : sec:HEL,                                            #
# prof_version   : 2000.02                                             #
#                                                                      #
# ....................                                                 #
# Copyright & Contact:                                                 #
# ....................                                                 #
#                                                                      #
# -> Copyright:Burkhard Rost, CUBIC NYC / LION Heidelberg              #
# -> Email:    rost@columbia.edu                                       #
# -> WWW:      http://cubic.bioc.columbia.edu                          #
# -> Fax:      +1-212-305 3773                                         #
#                                                                      #
# .............                                                        #
# Please quote:                                                        #
# .............                                                        #
#                                                                      #
# -> PROF:      B Rost (1996) Methods in Enzymology, 266:525-539       #
# -> PROFsec:   B Rost & C Sander (1993) J Mol Biol, 232:584-599       #
#                                                                      #
#                                                                      #
# -------------------------------------------------------------------- #
# ABBREVIATIONS used:                                                  #
# -------------------------------------------------------------------- #
#                                                                      #
# AA        : amino acid sequence                                      #
# OBS_sec   : observed secondary structure: H=helix, E=extended        #
#             (sheet), blank=other (loop)                              #
# PROF_sec  : PROF predicted secondary structure: H=helix, E=extended  #
#             (sheet), blank=other (loop)                              #
#             PROF = PROF: Profile network prediction HeiDelberg       #
# Rel_sec   : reliability index for PROFsec prediction (0=low          #
#             to 9=high)                                               #
#             Note: for the brief presentation strong predictions      #
#             marked by '*'                                            #
# SUB_sec   : subset of the PROFsec prediction, for all residues       #
#             with an expected average accuracy > 82% (tables          #
#             in header)                                               #
#             NOTE: for this subset the following symbols are used:    #
#               L: is loop (for which above ' ' is used)               #
#               .: means that no prediction is made for this           #
#             residue, as the reliability is:  Rel < 5                 #
#  pH_sec   : 'probability' for assigning helix (1=high, 0=low)        #
#  pE_sec   : 'probability' for assigning strand (1=high, 0=low)       #
#  pL_sec   : 'probability' for assigning neither helix, nor           #
#             strand (1=high, 0=low)                                   #
#                                                                      #
# prot_id   : identifier of protein [w]                                #
# prot_name : name of protein [w]                                      #
# prot_nres : number of residues [d]                                   #
# prot_nali : number of proteins aligned in family [d]                 #
# prot_nchn : number of chains (if PDB protein) [d]                    #
# prot_nfar : number of distant relatives [d]                          #
# ali_orig  : input file                                               #
# ali_used  : input file used                                          #
# ali_para  : parameters used to filter input file                     #
# prof_fpar : name of parameter file, used [w]                         #
# prof_nnet : number of networks used for prediction [d]               #
# prof_mode : mode of prediction [w]                                   #
# prof_version: version of PROF                                        #
# prof_skip : note: sequence stretches with less than 9 are            #
#             not predicted, the symbol '*' is used!                   #
# ali_pide  : percentage of identical residues                         #
# ali_lali  : number of aligned residues                               #
#                                                                      #
#                                                                      #
# ==================================================================== #
# PROF_BODY with predictions for 1ppt                                  #
# ==================================================================== #
#                                                                      #
# --------------------
# PROF results (brief)
# --------------------
# 
                   
                   ....,....1....,....2....,....3....,....4 
         AA       |GPSQPTYPGDDAPVEDLIRFYDNLQQYLNVVTRHRY|
         OBS_sec  |                                    |
         PROF_sec |             HHHHHHHHHHHHHHHHHHH    |
         Rel_sec  |******************************     *|

# --------------------------------------------------------------------
# ---------------------
# PROF results (normal)
# ---------------------
# 
                   
                   ....,....1....,....2....,....3....,....4 
         AA       |GPSQPTYPGDDAPVEDLIRFYDNLQQYLNVVTRHRY|
 alignment:                                             pide lali
   1 paho_chick   |GPSQPTYPGDDAPVEDLIRFYNDLQQYLNVVTRHRY|  94   36
   2 paho_strca   |GPAQPTYPGDDAPVEDLVRFYDNLQQYLNVVTRHRY|  94   36
   3 paho_larar   |GPVQPTYPGDDAPVEDLVRFYNDLQQYLNVVTRHRY|  89   36
   4 paho_allmi   |.PLQPKYPGDGAPVEDLIQFYDDLQQYLNVVTRPRF|  80   35
   5 paho_ansan   |GPSQPTYPGNDAPVEDLRFYYDNLQQYRLNVFRHRY|  78   36
   6 neuy_sheep   |.PSKPDNPGDDAPAEDLARYYSALRHYINLITRQRY|  60   35
   7 neuy_pig     |.PSKPDNPGEDAPAEDLARYYSALRHYINLITRQRY|  56   35
   8 pyy_human    |.PIKPEAPGEDASPEELNRYYASLRHYLNLVTRQRY|  54   35
   9 neuy_rabit   |.PSKPDNPGEDAPAEDMARYYSALRHYINLITRQRY|  54   35
  10 pyy_pig      |.PAKPEAPGEDASPEELSRYYASLRHYLNLVTRQRY|  54   35
  11 neuy_chick   |.PSKPDSPGEDAPAEDMARYYSALRHYINLITRQRY|  54   35
  12 pyy_rat      |.PAKPEAPGEDASPEELSRYYASLRHYLNLVTRQRY|  54   35
  13 neuy_rat     |.PSKPDNPGEDAPAEDMARYYSALRHYINLITRQRY|  54   35
  14 neuy_human   |.PSKPDNPGEDAPAEDMARYYSALRHYINLITRQRY|  54   35
  15 pyy_amica    |.PPKPENPGEDAPPEELARYYTALRHYINLITRQRY|  51   35
  16 neuy_xenla   |.PSKPDNPGEDAPAEDMAKYYSALRHYINLITRQRY|  51   35
  17 neuy_torma   |.PSKPDNPGEGAPAEDLAKYYSALRHYINLITRQRY|  51   35
  18 neuy_ranri   |.PSKPDNPGEDAPAEDMAKYYSALRHYINLITRQRY|  51   35
  19 neuy_oncmy   |.PVKPENPGEDAPTEELAKYYTALRHYINLITRQRY|  49   35
  20 pyy_rajrh    |.PPKPENPGDDAAPEELAKYYSALRHYINLITRQRY|  49   35
  21 npy_lamfl    |.PNKPDSPGEDAPAEDLARYLSAVRHYINLITRQRY|  49   35
  22 pyy_lepsp    |.PPKPENPGEDAPPEELAKYYSALRHYINLITRQRY|  49   35
  23 pyy_oncki    |.PPKPENPGEDAPPEELAKYYTALRHYINLITRQRY|  49   35
  24 neuy_gadmo   |.PIKPENPGEDAPADELAKYYSALRHYINLITRQRY|  46   35
  25 paho_canfa   |.PLEPVYPGDDATPEQMAQYAAELRRYINMLTRPRY|  46   35
  26 paho_didma   |.PQEPVYPGDDATPEQMAKYAAELRRYINRLTRPRY|  46   35
  27 paho_rabit   |.PPEPVYPGDDATPEQMAEYVADLRRYINMLTRPRY|  46   35
  28 neuy_carau   |.PTKPDNPGEGAPAEELAKYYSALRHYINLITRQRY|  46   35
  29 paho_pig     |.PLEPVYPGDDATPEQMAQYAAELRRYINMLTRPRY|  46   35
  30 paho_rante   |.PSEPHHPGDQATQDQLAQYYSDLYQYITFVTRPRF|  46   35
  31 pyy_bovin    |.PAKPQAPGEHASPDELNRYYTSLRHYLNLVTRQRF|  46   35
 prediction:                                            
         OBS_sec  |                                    |
         PROF_sec |             HHHHHHHHHHHHHHHHHHH    |
         Rel_sec  |987787777688878888889888888877302248|
 detail: 
          pH_sec  |000000000000099999999999999988643210|
          pE_sec  |001101100100000000000000000000011220|
          pL_sec  |988888888889900000000000000011245569|
 subset: SUB_sec  |LLLLLLLLLLLLLHHHHHHHHHHHHHHHHH.....L|


# --------------------------------------------------------------------
# ----------------------------------------
# PROF results (detailed ASCII 'graphics')
# ----------------------------------------
# 
                   
                   ....,....1....,....2....,....3....,....4 
         AA       |GPSQPTYPGDDAPVEDLIRFYDNLQQYLNVVTRHRY|
         OBS_sec  |                                    |
                  +------------------------------------+
 graph:   pH_sec  |             ###############        |  1.0 
 graph:   pH_sec  |             #################      |  0.9 
 graph:   pH_sec  |             #################      |  0.8 
 graph:   pH_sec  |             ##################     |  0.7 
 graph:   pH_sec  |             ##################     |  0.6 
 graph:   pH_sec  |             ###################    |  0.5 
 graph:   pH_sec  |             ####################   |  0.4 
 graph:   pH_sec  |             #####################  |  0.3 
 graph:   pH_sec  |             ###################### |  0.2 
                  +------------------------------------+
                  +------------------------------------+
 graph:   pE_sec  |                                    |  1.0 
 graph:   pE_sec  |                                    |  0.9 
 graph:   pE_sec  |                                    |  0.8 
 graph:   pE_sec  |                                    |  0.7 
 graph:   pE_sec  |                                    |  0.6 
 graph:   pE_sec  |                                    |  0.5 
 graph:   pE_sec  |                                    |  0.4 
 graph:   pE_sec  |                                 ## |  0.3 
 graph:   pE_sec  |  ## ##  #                     #### |  0.2 
                  +------------------------------------+
                  +------------------------------------+
 graph:   pL_sec  |#          ##                      #|  1.0 
 graph:   pL_sec  |#############                      #|  0.9 
 graph:   pL_sec  |#############                      #|  0.8 
 graph:   pL_sec  |#############                     ##|  0.7 
 graph:   pL_sec  |#############                   ####|  0.6 
 graph:   pL_sec  |#############                  #####|  0.5 
 graph:   pL_sec  |#############                  #####|  0.4 
 graph:   pL_sec  |#############                 ######|  0.3 
 graph:   pL_sec  |#############               ########|  0.2 
                  +------------------------------------+

# --------------------------------------------------------------------

