# Perl-RDB 
# PROFboth
# 
# Copyright          : Burkhard Rost, CUBIC NYC / LION Heidelberg
# Email              : rost@columbia.edu
# WWW                : http://cubic.bioc.columbia.edu
# Version            : 2000.02
# 
# --------------------------------------------------------------------------------
# About your protein :
# 
# VALUE    PROT_ID   : unk
# VALUE    PROT_NRES : 36
# VALUE    PROT_NALI : 1
# VALUE    PROT_NFAR : 0
# 
# --------------------------------------------------------------------------------
# About the alignment:
# 
# VALUE    ALI_ORIG  : 1ppt.f
# 
# --------------------------------------------------------------------------------
# About PROF specifics:
# 
# VALUE    PROF_FPAR : acc=/usr/share/profphd/prof/net/PROFboth_best.par
# VALUE    PROF_NNET : acc=6
# 
# --------------------------------------------------------------------------------
# Notation used      :
# 
# ------------------------------------------------------------------------
# NOTATION HEADER    : PROTEIN
# NOTATION PROT_ID   : identifier of protein [w]
# NOTATION PROT_NRES : number of residues [d]
# NOTATION PROT_NALI : number of proteins aligned in family [d]
# NOTATION PROT_NFAR : number of distant relatives [d]
# 
# ------------------------------------------------------------------------
# NOTATION HEADER    : ALIGNMENT
# NOTATION HEADER    : ALIGNMENT: input file
# 
# ------------------------------------------------------------------------
# NOTATION HEADER    : INTERNAL
# NOTATION PROF_FPAR : name of parameter file, used [w]
# NOTATION PROF_NNET : number of networks used for prediction [d]
# 
# 
# ------------------------------------------------------------------------
# NOTATION BODY      : PROTEIN
# NOTATION NO        : counting residues [d]
# NOTATION AA        : amino acid one letter code [A-Z!a-z]
# NOTATION CHN       : protein chain [A-Z!a-z]
# 
# ------------------------------------------------------------------------
# NOTATION BODY      : PROF
# 
# ------------------------------------------------------------------------
# NOTATION BODY      : PROFsec
# NOTATION PHEL      : PROF predicted secondary structure: H=helix, E=extended (sheet), blank=other (loop) PROF = PROF: Profile network prediction HeiDelberg
# NOTATION RI_S      : reliability index for PROFsec prediction (0=lo 9=high) Note: for the brief presentation strong predictions marked by '*'
# NOTATION pH        : 'probability' for assigning helix (1=high, 0=low)
# NOTATION pE        : 'probability' for assigning strand (1=high, 0=low)
# NOTATION pL        : 'probability' for assigning neither helix, nor strand (1=high, 0=low)
# NOTATION OtH       : actual neural network output from PROFsec for helix unit
# NOTATION OtE       : actual neural network output from PROFsec for strand unit
# NOTATION OtL       : actual neural network output from PROFsec for 'no-regular' unit
# 
# ------------------------------------------------------------------------
# NOTATION BODY      : PROFacc
# NOTATION PACC      : PROF predicted solvent accessibility (acc) in square Angstroem
# NOTATION PREL      : PROF predicted relative solvent accessibility (acc) in 10 states: a value of n (=0-9) corresponds to a relative acc. of between n*n % and (n+1)*(n+1) % (e.g. for n=5: 16-25%).
# NOTATION RI_A      : reliability index for PROFacc prediction (0=low to 9=high) Note: for the brief presentation strong predictions marked by '*'
# NOTATION Pbe       : PROF predicted  relative solvent accessibility (acc) in 2 states: b = 0-16%, e = 16-100%.
# NOTATION Pbie      : PROF predicted relative solvent accessibility (acc) in 3 states: b = 0-9%, i = 9-36%, e = 36-100%.
# NOTATION Ot4       : actual neural network output from PROFsec for unit 0 coding for a relative solvent accessibility of 4*4 - 5*5 percent (16-25%). Note: OtN, with N=0-9 give the same information for the other output units!
# 
# --------------------------------------------------------------------------------
# 
No	AA	PHEL	RI_S	PACC	PREL	RI_A	pH	pE	pL	Pbe	Pbie	OtH	OtE	OtL	Ot0	Ot1	Ot2	Ot3	Ot4	Ot5	Ot6	Ot7	Ot8	Ot9
1	G	L	9	75	90	8	0	0	9	e	e	1	2	96	0	0	1	3	5	9	15	22	37	44
2	P	L	7	122	90	4	0	0	8	e	e	7	7	86	0	1	3	6	10	14	19	24	33	35
3	S	L	7	93	72	3	0	1	8	e	e	5	11	86	1	2	4	9	13	18	23	25	28	27
4	Q	L	7	142	72	2	0	1	8	e	e	4	10	86	2	3	6	10	14	18	22	23	25	23
5	P	L	8	97	72	2	0	0	8	e	e	3	8	89	2	3	6	10	14	17	21	22	25	24
6	T	L	7	102	72	2	0	1	8	e	e	3	11	87	2	3	6	10	14	17	20	22	24	23
7	Y	L	7	44	20	0	0	1	8	e	i	2	14	87	3	5	10	15	17	17	17	16	16	15
8	P	L	8	97	72	2	0	0	8	e	e	4	6	88	2	3	5	9	13	17	20	22	25	25
9	G	L	8	75	90	2	0	0	8	e	e	5	6	88	1	2	5	9	13	16	19	22	27	28
10	D	L	7	117	72	3	0	0	8	e	e	5	9	87	0	1	3	7	12	16	21	24	29	29
11	D	L	8	117	72	4	0	0	8	e	e	7	4	87	0	1	3	7	11	16	22	27	32	32
12	A	L	7	44	42	1	1	0	8	e	e	13	3	85	3	5	8	13	17	18	20	19	19	16
13	P	L	3	57	42	5	3	0	6	e	e	34	1	67	2	3	7	12	20	26	30	25	16	9
14	V	H	7	59	42	3	8	0	1	e	e	86	1	15	4	5	8	11	16	19	23	22	18	13
15	E	H	7	108	56	5	8	0	1	e	e	88	1	13	0	1	3	7	12	18	27	30	25	19
16	D	H	7	68	42	4	8	0	1	e	e	89	1	11	4	5	8	12	17	22	26	24	16	10
17	L	H	8	32	20	1	9	0	0	e	i	91	1	6	11	11	13	16	18	18	17	13	8	4
18	I	H	8	70	42	2	9	0	0	e	e	90	2	5	9	10	12	15	18	20	22	19	12	7
19	R	H	8	104	42	6	8	0	0	e	e	88	2	8	1	2	5	9	15	22	30	28	19	10
20	F	H	8	82	42	1	9	0	0	e	e	89	1	7	12	12	13	15	17	18	19	15	11	6
21	Y	H	8	93	42	1	9	0	0	e	e	89	0	7	8	9	12	14	16	18	19	17	12	8
22	D	H	7	68	42	3	9	0	0	e	e	88	0	9	2	3	5	8	13	20	27	27	22	15
23	N	H	7	65	42	4	8	0	1	e	e	87	0	12	3	4	7	11	16	20	25	23	17	10
24	L	H	8	32	20	1	9	0	0	e	i	91	0	7	12	14	16	19	20	19	17	12	8	4
25	Q	H	8	110	56	3	9	0	0	e	e	89	1	8	2	3	6	9	13	17	24	26	21	15
26	Q	H	7	110	56	4	8	0	0	e	e	87	1	9	2	3	5	7	11	17	24	27	24	19
27	Y	H	6	93	42	0	8	0	1	e	e	80	4	12	9	10	12	14	16	16	17	15	12	9
28	L	H	5	32	20	1	7	1	1	e	i	72	11	14	13	14	15	17	18	17	16	13	10	7
29	N	H	4	87	56	3	6	1	2	e	e	65	14	22	4	5	7	9	13	17	22	23	20	15
30	V	H	4	59	42	1	6	1	1	e	e	62	20	19	8	9	12	15	17	18	20	18	15	12
31	V	H	1	59	42	0	4	2	2	e	e	47	30	29	11	12	14	17	19	19	20	17	14	11
32	T	H	1	59	42	1	4	2	3	e	e	41	29	31	7	8	10	13	17	19	21	20	18	16
33	R	E	0	104	42	1	3	3	3	e	e	33	36	31	2	3	6	10	16	19	23	23	21	18
34	H	E	1	77	42	0	2	4	2	e	e	25	47	29	4	5	9	13	17	19	21	20	20	19
35	R	E	0	178	72	1	2	4	3	e	e	25	43	37	1	2	5	9	14	18	22	23	24	23
36	Y	L	8	199	90	7	0	0	8	e	e	6	7	88	1	1	2	4	7	9	14	19	32	38
