Summary: secondary structure and solvent accessibility predictor
Name: profphd
Version: 1.0.38
Release: 1
License: GPL
Group: Applications/Science
Source: ftp://rostlab.org/%{name}/%{name}-%{version}.tar.gz
URL: http://rostlab.org/
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-root
Requires: librg-utils-perl >= 1.0.16, pp-popularity-contest, profnet-prof, profphd-utils, profnet-phdnet

%description
 Solvent accessibility is predicted by a neural network method rating at a
 correlation coefficient (correlation between experimentally observed and
 predicted relative solvent accessibility) of 0.54 cross-validated on a set of
 238 globular proteins (Rost & Sander, Proteins, 1994, 20, 216-226;
 evaluation of accuracy). The output of the neural network codes for 10 states
 of relative accessibility. Expressed in units of the difference between
 prediction by homology modelling (best method) and prediction at random
 (worst method), PROFacc is some 26 percentage points superior to a comparable
 neural network using three output states (buried, intermediate, exposed) and
 using no information from multiple alignments.
 .
 Transmembrane helices
 in integral membrane proteins are predicted by a system of neural networks.
 The shortcoming of the network system is that often too long helices are
 predicted. These are cut by an empirical filter. The final prediction
 (Rost et al., Protein Science, 1995, 4, 521-533; evaluation of accuracy)
 has an expected per-residue accuracy of about 95%. The number of false
 positives, i.e., transmembrane helices predicted in globular proteins, is
 about 2%.
 .
 The neural network prediction of transmembrane helices
 (PHDhtm) is refined by a dynamic programming-like algorithm. This method
 resulted in correct predictions of all transmembrane helices for 89% of the
 131 proteins used in a cross-validation test; more than 98% of the
 transmembrane helices were correctly predicted. The output of this method
 is used to predict topology, i.e., the orientation of the N-term with respect
 to the membrane. The expected accuracy of the topology prediction is > 86%.
 Prediction accuracy is higher than average for eukaryotic proteins and lower
 than average for prokaryotes. PHDtopology is more accurate than all other
 methods tested on identical data sets.%{common_desc}

%prep
%setup -q

# lkajan: correct but unnecessary detection of perl(lib-ut.pl) dependency - fix it: https://fedoraproject.org/wiki/EPEL:Packaging#Perl_Provides_and_Requires_Filtering
cat << \EOF > %{name}-req
#!/bin/sh
%{__perl_requires} $* |\
sed -e '/perl(lib-ut.pl)/d'
EOF

%global __perl_requires %{_builddir}/%{name}-%{version}/%{name}-req
chmod +x %{__perl_requires}

%build
make prefix=%{_prefix}

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=%{buildroot} prefix=%{_prefix} install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc AUTHORS
%doc COPYING
%doc README
%{_bindir}/
%{_mandir}/*/*
%{_datadir}/%{name}/

%changelog
* Fri Jan 13 2012 Laszlo Kajan <lkajan@rostlab.org> - 1.0.38-1
- one-package version in upstream
* Tue Sep 27 2011 Laszlo Kajan <lkajan@rostlab.org> - 1.0.35-1
- one-package version in upstream
* Tue Jul 5 2011 Laszlo Kajan <lkajan@rostlab.org> - 1.0.34-3
- requires pp-popularity-contest
* Tue Jun 21 2011 Laszlo Kajan <lkajan@rostlab.org> - 1.0.34-2
- moved spec to dist root
* Fri Jun 17 2011 Laszlo Kajan <lkajan@rostlab.org> - 1.0.34-1
- First rpm package
